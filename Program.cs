﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using System.Linq;

namespace RipassoGeneraleUno
// -Struct:
//2) Definire una struct persona con nome e altezza(in centimetri). 
//Generare poi 10 persone, il cui nome sarà estratto casualmente da un elenco di 5 nomi, 
//    e l'altezza sarà un numero casuale tra 140 e 200. Trovare e comunicare poi il nome della persona più alta e di quella più bassa
{
    class Program
    {

        static void Main(string[] args)
        {
            string[] names = new string[] { "Mario", "Ludovica", "Paride", "Leo", "Chiara" };
            int[] heigh = new int[10];
            int arrayLong = heigh.Length;
            Random generatore = new Random();

            for (int i = 0; i < arrayLong; i++)
            {
                int casualHeigh = generatore.Next(140, 201);
                heigh[i] = casualHeigh;
                Console.WriteLine("L'altezza numero {0}° del vettore è {1}", i + 1, heigh[i]);
            }

            Persona[] persona = new Persona[10];
            arrayLong = persona.Length;
            int arrayTwoLong = names.Length;

            for (int i = 0; i < arrayLong; i++)
            {
                persona[i].name = names[generatore.Next(arrayTwoLong)];
                persona[i].heigh = heigh[i];
                Console.WriteLine("Il nome della {0} ° persona è {1} mentre l'altezza è {2}", i + 1, persona[i].name, persona[i].heigh);
            }

            int smallest = persona[0].heigh;
            string smallestName = "Pippo";

            for (int i = 0; i < arrayLong; i++)
            {
                if (smallest > persona[i].heigh)
                {
                    smallest = persona[i].heigh;
                    smallestName = persona[i].name;
                }
            }

            int tallest = persona[0].heigh;
            string tallestName = "Federica";

            for (int i = 0; i < arrayLong; i++)
            {
                if (tallest < persona[i].heigh)
                {

                    tallest = persona[i].heigh;
                    tallestName = persona[i].name;
                }
            }

            Console.WriteLine("La persona più bassa misura {0}cm e si chiama {1}, mentre quella piu' alta misura {2}cm e si chiama {3}", smallest, smallestName, tallest, tallestName);
        }

        struct Persona
        {
            public int heigh;
            public string name;
            public Persona(int heigh, string name)
            {
                this.heigh = heigh;
                this.name = name;
            }
        }
    }
}

  //creare struct di int altezza e string nome
        //generare 10 persone
            //creare ed inserire nome(massimo 5) e altezza(10 compresa tra 140 e 200)
                //stampare nome e altezza delle persone
    //controllo quale persona è più bassa
    //controllo quale persona è più alta
        //dichiarare  il nome della persona più alta e più bassa